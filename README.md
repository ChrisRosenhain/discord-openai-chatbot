# discord-openai-chatbot

A discord chatbot that uses OpenAI's GPT-3 API to generate responses.

It's terrible in every way, but it's fun to play with.

You'll need an OpenAI account, and organisation ID, and an API key. You can get one here: https://platform.openai.com/

You'll also need a Discord developer account, your guild ID, and a bot token. You can get one here: https://discord.com/developers/applications

Copy config.ini.sample to config.ini and fill in the fields.

I never intended for anyone else to actually use this code, so it's a bit of a mess. I'll probably never clean it up. Sorry.